1 FROM python:3
2 ENV PYTHONUNBUFFERED 1
3 WORKDIR /code
4
5 COPY . /code/
6
7 RUN pip install -r requirements.txt
8
9 VOLUME ["/code/db"]
10 EXPOSE 8000
11 CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000

